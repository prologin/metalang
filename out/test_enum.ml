type foo_t = 
    Foo
  | Bar
  | Blah

let () =
begin
  let _foo_val = Foo in ()
end
 